$(document).ready(function(){
  var form_count = 1, previous_form, next_form, total_forms;
  total_forms = $("fieldset").length;
  $(".next-form").click(function(){
    previous_form = $(this).parents("fieldset:first");
    next_form = $(this).parents("fieldset:first").next();
    next_form.show();
    previous_form.hide();
    setProgressBarValue(++form_count);
  });
  $(".previous-form").click(function(){
    previous_form = $(this).parent();
    next_form = $(this).parent().prev();
    next_form.show();
    previous_form.hide();
    setProgressBarValue(--form_count);
  });
  setProgressBarValue(form_count);
  function setProgressBarValue(value){
    var percent = parseFloat(100 / total_forms) * value;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")

  }

  // Handle form submit and validation
  $( "#register_form" ).submit(function(event) {
    let is_valid = true;

    if(!$("#nom").val()) {
      document.getElementById('error-name').innerHTML = "Entrez votre nom";
      is_valid = false
    } else {
      document.getElementById('error-name').innerHTML = "";
    }

    if(!$("#prenom").val()) {
      document.getElementById('error-prenom').innerHTML = "Entrez votre prénom";
      is_valid = false
    } else {
      document.getElementById('error-prenom').innerHTML = "";
    }

    const email = $("#email").val();
    if (!email || !checkEmail(email)) {
      document.getElementById('error-email').innerHTML = "Entrez une adresse mail valide";
      is_valid = false
    } else {
      document.getElementById('error-email').innerHTML = "";
    }

    const tel = $("#tel").val();
    if(!tel || !checkTel(tel)) {
      document.getElementById('error-tel').innerHTML = "Entrez un numéro de téléphone valide";
      is_valid = false
      console.log('test')
    } else {
      console.log('test')
      document.getElementById('error-tel').innerHTML = "";
    }


    if (!is_valid)
      return is_valid
  });

  function checkTel(tel_tmp) {
    var tel_re = /^(0)[1-9](\d{2}){4}$/;
    return tel_re.test(tel_tmp);
  }

  function checkEmail(email) {
    var re_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re_email.test(email);
  }

});