<?php
//export.php
include_once("db_connect.php");
if(isset($_POST["export"]))
{
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=data.csv');
    $output = fopen("php://output", "w");
    fputcsv($output, array('id', 'langue', 'metier', 'temps', 'nom', 'prenom', 'email', 'tel', 'date', 'contact', 'eligibilite','id_cp'));
    $sql = "SELECT * from info GROUP BY id ORDER BY id DESC ";
    if (isset($db)) {
        $query = $db->query($sql, PDO::FETCH_ASSOC);
        while ($result = $query->fetch())
        {
            fputcsv($output, $result);
        }
        fclose($output);
    }
}
?>