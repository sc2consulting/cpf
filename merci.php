<?php
include_once("db_connect.php");


if (isset($_POST['submit'])) {
	$stmt = $db->prepare('INSERT INTO info(nom, prenom, email, tel, contact, langue, metier, temps, eligibilite, id_cp) VALUES (:nom, :prenom, :email, :tel, :contact, :langue, :metier, :temps, :eligibilite, :id_cp)');

	$stmt->bindParam(':nom', $nom);
	$stmt->bindParam(':prenom', $prenom);
	$stmt->bindParam(':email', $email);
	$stmt->bindParam(':tel', $tel);
	$stmt->bindParam(':contact', $contact);
	$stmt->bindParam(':langue', $langue);
	$stmt->bindParam(':metier', $metier);
	$stmt->bindParam(':temps', $temps);
	$stmt->bindParam(':eligibilite', $eligibilite);
	$stmt->bindParam(':id_cp', $id_cp);

	$nom = htmlspecialchars($_POST['nom']);
	$prenom = htmlspecialchars($_POST['prenom']);
	$email = htmlspecialchars($_POST['email']);
	$tel = htmlspecialchars($_POST['tel']);
	$contact = htmlspecialchars($_POST['contact']);
	$langue = htmlspecialchars($_POST['langue']);
	$metier = htmlspecialchars($_POST['eligibilite']);
	$temps = htmlspecialchars($_POST['temps']);
	if ($metier == 'public_salarie') {
		$eligibilite = 'no';
	}elseif ($metier == 'retraite_plus3'){
		$eligibilite = 'no';
	}elseif ($metier == 'etudiant'){
		$eligibilite = 'no';
	}elseif ($temps == 'moins_1'){
		$eligibilite ='no';
	}else {
		$eligibilite = 'yes';
	}

	$id_cp = 1;
	$query = "SELECT * FROM info WHERE tel = " . $tel;
	$result = $db->query($query)->fetchAll();
	if ($result != NULL) {
		echo file_get_contents("deja_inscrit.php");
	}else {
		if (!(!$nom || !$prenom || !$email || !$tel || !$contact || !$langue || !$metier || !$temps)) {
			$stmt->execute();
			if ($eligibilite == 'yes'){
				//header('Location: thanks.php');
				echo file_get_contents("thanks_eligibilite.php");
			}elseif ($eligibilite == 'no'){
				//header('Location: confirmation.php');
				echo file_get_contents("confirmation_non_eligible.php");

			}
		}
	}
}
?>