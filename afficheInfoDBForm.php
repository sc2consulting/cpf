<?php
include_once ('db_connect.php');
/** @var PDO $db */

?>

<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
</head>
<body style="padding: 30px">

<h1>Connexion</h1>
<form name="form" method="post">
    <p><label for="password">Mot de passe</label> <input type="password" title="Saisissez le mot de passe" name="password" /></p>
    <p><input type="submit" name="submit" value="Connexion" /></p>
</form>

<?php
if (isset($_POST['submit'])) {
	if (isset($_POST['password']) AND $_POST['password'] ==  "ElBenyamin1991!") // Si le mot de passe est bon
	{
		?>
        <div class="container" style="width: 100%">
            <form method="post" action="export.php">
                <input type="submit" name="export" value="Sortir en CSV" class="btn btn-danger">
            </form>

            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr class="btn-primary">
                    <!--            <th>id</th>-->
                    <!--            <th>Eligible</th>-->
                    <!--            <th>langue</th>-->
                    <!--            <th>metier</th>-->
                    <!--            <th>temps</th>-->
                    <!--            <th>Nom</th>-->
                    <!--            <th>Prénom</th>-->
                    <!--            <th>Email</th>-->
                    <!--            <th>Telephone</th>-->
                    <!--            <th>Contact</th>-->
                    <!--            <th>Date</th>-->
                    <!--            <th>id_cp</th>-->
                    <td>date d'inscription</td>
                    <td>nom</td>
                    <td>prénom</td>
                    <td>email</td>
                    <td>langue</td>
                    <td>contact</td>
                    <!--<td>temps préfeéré</td>-->
                    <td>type métier</td>
                    <td>temps travaillé</td>
                    <td>éligibilité</td>
                </tr>
                </thead>
                <tbody>
				<?php
				$sql = "select * from info";
				$query = $db->query($sql);
				$i = 1;
				while ($row = $query->fetch()) // fetch all data from the database
				{
					?>
                    <tr>
                        <!--                <td>--><?php //echo $row['id']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['eligibilite']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['langue']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['metier']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['temps']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['nom']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['prenom']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['email']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['tel']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['contact']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['date']; ?><!--</td>-->
                        <!--                <td>--><?php //echo $row['id_cp']; ?><!--</td>-->
                        <td><?php echo $row['date']; ?></td>
                        <td><?php echo $row['nom']; ?></td>
                        <td><?php echo $row['prenom']; ?></td>
                        <td><?php echo $row['email']; ?></td>
                        <td><?php echo $row['langue']; ?></td>
                        <td><?php echo $row['tel']; ?></td>
                        <!-- <td>--> <?php //echo $row['contact']; ?><!--</td>-->
                        <td><?php echo $row['metier']; ?></td>
                        <td>
							<?php
							if ($row['temps'] === 'moins_1')
								echo 'moins 1 an';
							if ($row['temps'] === '1_3')
								echo '1 à 3 ans';
							if ($row['temps'] === 'plus_3')
								echo '3 ans';
                            if ($row['temps'] === 'plus_1')
								echo 'plus 1 ans';
							?>
                        </td>

                        <td><?php echo $row['eligibilite']; ?></td>
                    </tr>

					<?php
					$i++;
				}
				?>
                </tbody>
            </table>
        </div>
		<?php
	} else {
		echo '<p>Mot de passe incorrect</p>';
	}
}
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
</body>
</html>